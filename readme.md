Moteur javascript pour la création d'applications de classement de listes d'éléments par drag & drop.

Les éléments à ordonner sont saisis dans le tableau du script items.js:

```var items = [
  ["absence","boîte","canari","lumière","wagon"],
  ["doigt","mallette","orange","poussière","table"]
  ];
  ```

  Il s'agit d'un tableau de tableaux dans lequel le 1er niveau correspond à une nouvelle liste, le second niveau aux éléments de la liste.
  Les éléments doivent être saisis dans l'ordre auquel on veut aboutir. Le script se chargera de les proposer dans un ordre aléatoire.
  Il n'y a pas de limites pour le nombre de listes, ni pour le nombre d'éléments de chaque liste.

  Pour créer une liste supplémentaire, ajouter une virgule à la fin du tableau de la ligne précédente, et ajouter le nouveau tableau avec ses éléments.

  Exemple:

  ```var items = [
  ["absence","boîte","canari","lumière","wagon"],
  ["doigt","mallette","orange","poussière","table"],
  ["baignoire","bain","bicyclette","brindille","brumisateur","brunir"],
  ["105","110","115","150","151","155"]
  ];
  ```

  Le script créera automatiquement les boutons de sélection de liste.

## Créer très simplement ses listes
Vous pouvez également beaucoup plus simplement créer vos listes à ordonner en utilisant le fichier Libreoffice Calc creer-listes.ods du répertoire.
Saisissez vos items dans le tableau du classeur, et cliquez sur le bouton "Créer fichier items.js". Le fichier sera automatiquement construit avec la bonne syntaxe.

Merci à zoom61 pour cette fonctionnalité.
