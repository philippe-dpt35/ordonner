/*
Entrer ici les items à classer.
Chaque nouveau tableau correspond à une nouvelle liste.
Pour créer une nouvelle liste, il suffit d'ajouter un nouveau tableau
sans oublier de mettre une vrigule à la fin de la ligne du tableau précédent.
Dans le tableau, les items doivent être impérativement saisis dans
l'ordre de classement. Le script se chargera de les proposer dans un ordre aléatoire.
*/
const items = [
  ["absence","boîte","canari","lumière","wagon"],
  ["doigt","mallette","orange","poussière","table"],
  ["baignoire","bain","bicyclette","brindille","brumisateur","brunir"],
  ["105","110","115","150","151","155"]
  ];
